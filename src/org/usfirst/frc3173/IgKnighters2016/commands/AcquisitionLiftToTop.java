package org.usfirst.frc3173.IgKnighters2016.commands;

import org.usfirst.frc3173.IgKnighters2016.Robot;

import edu.wpi.first.wpilibj.command.Command;

/**
 *
 */
public class AcquisitionLiftToTop extends Command {

    public AcquisitionLiftToTop() {
        // Use requires() here to declare subsystem dependencies
        // eg. requires(chassis);
    }

    // Called just before this Command runs the first time
    protected void initialize() {
    	Robot.acquisition.liftAcquisitionBelt(Robot.acquisition.LIFT_SPEED);
    }

    // Called repeatedly when this Command is scheduled to run
    protected void execute() {
    }

    // Make this return true when this Command no longer needs to run execute()
    protected boolean isFinished() {
        return Robot.acquisition.isUpperLimitHit();
    }

    // Called once after isFinished returns true
    protected void end() {
    	Robot.acquisition.liftAcquisitionBelt(0);
    }

    // Called when another command which requires one or more of the same
    // subsystems is scheduled to run
    protected void interrupted() {
    }
}
