package org.usfirst.frc3173.IgKnighters2016.commands;

import org.usfirst.frc3173.IgKnighters2016.Robot;

import edu.wpi.first.wpilibj.command.CommandGroup;

public class AutoShoot extends CommandGroup {
	public AutoShoot(){
		addSequential(new AutoShootBegin());
		addSequential(new AutoShootSpinUp());
		addSequential(new AutoShootLeftRight());
		addSequential(new AutoShootForwardBack());
		addSequential(new AutoShootLeftRight());
		addSequential(new AutoShootCheckSpeed());
		addSequential(new AutoShootBelt());
		addSequential(new doNothin(),.5);
		addSequential(new AutoShootFinish());
		this.cancel();
	}
}
