package org.usfirst.frc3173.IgKnighters2016.commands;

import edu.wpi.first.wpilibj.command.Command;
import org.usfirst.frc3173.IgKnighters.utilities.IMU;
import org.usfirst.frc3173.IgKnighters2016.Robot;

public class AutomaticTurn extends Command {
	public double rotat=0;
	public static IMU RRIMU=null;
	public double ini=0;
	
	
	public AutomaticTurn(double rotation){
		requires(Robot.drives);
		rotat=rotation;	
	}
	
	@Override
	protected void initialize() {
		// TODO Auto-generated method stub
		RRIMU.calibration();
		ini=RRIMU.getOrientationZ();
		if(rotat<=180){
			Robot.drives.drivePercentVbus(-1,0);
		}
		else{
			Robot.drives.drivePercentVbus(0,-1);
		}
	}

	@Override
	protected void execute() {
		// TODO Auto-generated method stub
		//if((RRIMU.getOrientationZ()+rotat)>){
		
		if(((ini+rotat)%360)+10>=RRIMU.getOrientationZ()&&((ini+rotat)%360)-10<=RRIMU.getOrientationZ()){
			Robot.drives.drivePercentVbus(0,0);
		}
		//}
	}

	@Override
	protected boolean isFinished() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	protected void end() {
		// TODO Auto-generated method stub
		Robot.drives.drivePercentVbus(0, 0);
	}

	@Override
	protected void interrupted() {
		// TODO Auto-generated method stub
		Robot.drives.drivePercentVbus(0, 0);
	}

}
